package com.example.practice.api;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest
class E_OnlyWebLayerTested_IT {

    @Autowired
    MockMvc mockMvc;

    @Test
    void when_get_request_received_then_returning_1() throws Exception {
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get("http://localhost", Integer.class));

        result.andExpect(status().isOk())
                .andExpect(jsonPath("$", is(1)));
    }
}
