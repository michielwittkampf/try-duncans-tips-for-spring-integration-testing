package com.example.practice.api;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

// This test class will fail because the RestTemplate (and Tomcat server) is not available.
@SpringBootTest
class A_FullApplicationTestedWithoutTomcat_FailingBecauseOfMissingRestTemplate_IT {

    @Autowired
    TestRestTemplate restTemplate;

    @LocalServerPort
    Integer localServerPort;

    @Test
    void when_get_request_received_then_returning_1() {
        Integer answer = restTemplate.getForObject("http://localhost:" + localServerPort, Integer.class);

        assertThat(answer).isEqualTo(1);
    }
}
