package com.example.practice.api;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

/**
 * In this test I start a webserver (Tomcat). That works. But for this test and in general there doesn't seem to be much reasons to use a webserver (i.e. Tomcat) while testing.
 *
 * The documentation states:
 *
 * "Testing within a mocked environment
 * is usually faster than running with a full Servlet container. However, since mocking occurs at the Spring MVC layer, code that relies on lower-level
 * Servlet container behavior cannot be directly tested with MockMvc.
 * <p>
 * For example, Spring Boot’s error handling is based on the “error page” support provided by the Servlet container. This means that, whilst you can test
 * your MVC layer throws and handles exceptions as expected, you cannot directly test that a specific custom error page is rendered. If you need to test
 * these lower-level concerns, you can start a fully running server as described in the next section."
 *
 * Source: https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-testing-spring-boot-applications-testing-with-mock-environment
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class C_FullApplicationTestedWithTomcat_UsingRestTemplate_IT {

    @Autowired
    TestRestTemplate restTemplate;

    @LocalServerPort
    Integer localServerPort;

    @Test
    void when_get_request_received_then_returning_1() {
        Integer answer = restTemplate.getForObject("http://localhost:" + localServerPort, Integer.class);

        assertThat(answer).isEqualTo(1);
    }

}
