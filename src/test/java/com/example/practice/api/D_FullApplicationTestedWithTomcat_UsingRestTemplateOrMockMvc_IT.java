package com.example.practice.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class D_FullApplicationTestedWithTomcat_UsingRestTemplateOrMockMvc_IT {

    @Autowired
    TestRestTemplate restTemplate;

    @LocalServerPort
    Integer localServerPort;

    @Autowired
    MockMvc mockMvc;

    @Test
    void rest_template_based_test___when_get_request_received_then_returning_1() {   // RestTemplate and Tomcat server are available (see logging)
        Integer answer = restTemplate.getForObject("http://localhost:" + localServerPort, Integer.class);

        assertThat(answer).isEqualTo(1);
    }

    @Test
    void mock_mvc_based_test___when_get_request_received_then_returning_1() throws Exception {
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get("http://localhost", Integer.class));

        result.andExpect(status().isOk())
                .andExpect(jsonPath("$", is(1)));
    }
}
